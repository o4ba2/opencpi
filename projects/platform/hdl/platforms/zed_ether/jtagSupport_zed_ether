#!/bin/bash --noprofile

# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

################################################################################
# jtag support functions for picoevb:
# Be sure to write error messages to stderr since some funtions return
# data on stdout
################################################################################
source $OCPI_CDK_DIR/scripts/util.sh
func=$1
temp=$2
dir=`dirname $0`
#echo bitfile=$1 device=$2 part=$3 esn=$4 position=$5 temp=$6

setVarsFromMake $OCPI_CDK_DIR/include/hdl/xilinx.mk ShellIseVars=1 $verbose
source $OcpiXilinxVivadoDir/settings64.sh 1>&2
if test $? != 0; then
  echo Xilinx tools setup failed. 1>&2
  exit 1
fi

export vivado=vivado

function good {
  rm -f $temps $temp.log
  exit 0
}
function bad {
  rm -f $temps
  if test -f $temp.log; then
     echo Dump of $temp.log:
     echo "*******************************************************************************"
     cat $temp.log
     echo "*******************************************************************************"
  fi
  exit 1
}

################################################################################
# Now we do individual functions
################################################################################
case $func in
  (cables)
################ 
# BYPASS 
################ 
     TEST_FILE=/etc/udev/rules.d/52-xilinx-digilent-usb.rules
     if test ! -f $TEST_FILE; then
       echo Could not find UDEV rules file necessary for JTAG-over-USB usage: $TEST_FILE. Please rerun the Xilinx cable driver setup process. 1>&2
       exit 1 # don't use bad because we don't want to dump the log
     fi
     $vivado -mode batch -source /dev/stdin > $temp.log <<EOF
open_hw_manager
connect_hw_server -allow_non_jtag
puts "BEGIN-TARGET-LIST"
puts [get_hw_targets]
puts "END-TARGET-LIST"
disconnect_hw_server
EOF
     # Return the serial number (after final slash) separately as well as combined with the URL
     CABLES=`grep -v "^#" < $temp.log | sed -n '/^BEGIN-TARGET-LIST$/,/^END-TARGET-LIST$/{//!p}' | \
	     sed -e 's^/\([^/]*\)\$^/\1=\1~^'`
     if test $? != 0 -o "$CABLES" = ""; then
       echo No Xilinx USB JTAG cables found using the Xilinx \"vivado\" tool. 1>&2
       bad
     fi
     echo $CABLES
     good
   ;;
  (unlock)
#     echo "Telling iMPACT to clear all locks. If you are using a cable in another application, things may fail!" 1>&2
#     $imp >> $temp.unlock.log <<EOF
# cleancablelock
# exit
# EOF
    echo "Unlock okay"
    good
   ;;
  (part)
     port=$3
     part=$4
     echo Finding cable position of part \"$part\" on cable at port \"$port\" 1>&2
     $vivado -mode batch -source /dev/stdin > $temp.log <<EOF
open_hw_manager
connect_hw_server -allow_non_jtag
open_hw_target $port
puts "BEGIN-DEVICE-LIST"
puts [get_hw_devices "${part}_*"]
puts "END-DEVICE-LIST"
close_hw_target
disconnect_hw_server
EOF
     PARTS=`grep -v "^#" < $temp.log | sed -n '/^BEGIN-DEVICE-LIST$/,/^END-DEVICE-LIST$/{//!p}'`
     if test $? != 0 -o "$PARTS" = ""; then
       echo Error: did not find part $part in the JTAG chain for cable $port. 1>&2
       bad
     fi
     echo $PARTS
     good
    ;;
  (load)
    port=$3
    pos=$4
    bitfile=$5
    case $bitfile in
     (*.bit) ;;
     (*)
       echo "Error - expecting bit file"
       bad
       ;;
    esac
    
    echo starting vivado 
    $vivado -mode batch -source /dev/stdin > $temp.log <<EOF
open_hw_manager
connect_hw_server -allow_non_jtag
open_hw_target $port
set Device [lindex [get_hw_devices $pos] 0]
current_hw_device \$Device
refresh_hw_device -update_hw_probes false \$Device
set_property PROBES.FILE {} \$Device
set_property FULL_PROBES.FILE {} \$Device
set_property PROGRAM.FILE {$bitfile} \$Device
program_hw_devices \$Device
refresh_hw_device \$Device
close_hw_target
disconnect_hw_server
EOF
    vivado_status=$?

    if test $vivado_status != 0; then
      echo "Error: FPGA programming failed!"
      bad
    else
      echo "FPGA programming succeeded!"
    fi
    good
    ;;
esac
echo Unknown JTAG function $func 1>&2
exit 1
