### New feature description
(Describe the new feature or enhancement in meticulous detail, and why it is needed.)

### Testing requirements (Optional)
(Any special hardware needed for the development and testing of this feature?)

### Links/references (Optional)
(Links or references supporting the purposed feature)

### Acceptance criteria
(What would it take for you to feel this issue can be closed?)



### Auto label - do not delete
~"type::feature"