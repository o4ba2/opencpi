.. %%NAME-CODE%% %%AUTHORING_MODEL%% worker


.. _%%NAME-CODE%%-%%AUTHORING_MODEL%%-worker:


``%%NAME-CODE%%`` %%AUTHORING_MODEL%% Worker
============================================
This document is generated from this RCC worker's metadata and not from an authored documentation file.

Detail
------
.. ocpi_documentation_worker::

.. Skeleton comment: If not a HDL worker / implementation then the below
   section and directive should be deleted. This comment should be removed in
   the final version of this page.

Utilization
-----------
.. ocpi_documentation_utilization::
