# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# This makefile is for building assemblies and bitstreams.
# The assemblies get built according to the standard build targets
HdlMode:=assembly
Model:=hdl
include $(OCPI_CDK_DIR)/include/util.mk
$(if $(call DoShell,! echo $(CwdName) | grep -s '[A-Z]',Var1),\
  $(error "This HDL assembly, $(CwdName), has upper case letters in its name, which is not supported due to limitations of some tools."))
$(OcpiIncludeAssetAndParent)
ComponentLibraries+=$(OCPI_PROJECT_COMPONENT_LIBRARIES)
include $(OCPI_CDK_DIR)/include/hdl/hdl-make.mk

ifneq ($(MAKECMDGOALS),clean)
hdl: all # for convenience
# These next lines are similar to what worker.mk does
$(if $(wildcard $(CwdName).xml),,\
  $(error The XML for the assembly, $(CwdName).xml, is missing))
endif

override Workers:=$(CwdName)
override Worker:=$(Workers)
Worker_$(Worker)_xml:=$(Worker).xml
Worker_xml:=$(Worker).xml
# Not good, but we default the language differently here for compatibility.
# Also, since we are deferring include of hdl-pre.mk until after we process containers
# (and thus determine actual usable platforms), we need to set language variables
# here, calling hdl-language.mk "early"
OcpiLanguage:=$(call OcpiGetLanguage,$(Worker_xml))
ifndef OcpiLanguage
  OcpiLanguage:=verilog
endif
include $(OCPI_CDK_DIR)/include/hdl/hdl-language.mk
override LibDir=lib/hdl
# If HdlPlatforms is explicitly defined to nothing, then don't build containers.
ifeq ($(HdlPlatform)$(HdlPlatforms),)
  ifneq ($(origin HdlPlatforms),undefined)
    override Containers=
    override DefaultContainers=
  endif
endif
# Add to the component libraries specified in the assembly Makefile, if any,
# and also include those passed down from the "assemblies" level via ComponentLibrariesInternal
# THIS LIST IS FOR THE APPLICATION ASSEMBLY NOT FOR THE CONTAINER
# This is done here so that the build file + OWD parsing will succeed when this assembly
# sets ComponentLibraries in its Makefile.
# But it will need to be done again *after* the build/OWD files are processes in case
# the build/OWD XML file sets ComponentLibraries
# Override since they may be passed in from assemblies level
override XmlIncludeDirsInternal:=$(XmlIncludeDirs) $(XmlIncludeDirsInternal)
# Make sure the build and OWD files are processed early because there are make variables
# that will affect target processing in hdl-pre.mk
# For assemblies, these are generated from the OWD XML
ifeq ($(ShellHdlAssemblyVars)$(filter clean%,$(MAKECMDGOALS)),)
  override ComponentLibrariesInternal+=components adapters
  $(eval $(OcpiProcessBuildFiles))
  # These are done here (again) in case the build files or OWD mentions component libraries in XML
  override ComponentLibraries+= $(ComponentLibrariesInternal)
  override XmlIncludeDirsInternal:=$(XmlIncludeDirs) $(XmlIncludeDirsInternal)
endif
ifdef Container
  ifndef Containers
    Containers:=$(Container)
  endif
endif
HdlContName=$(Worker)_$1_$2
HdlContOutDir=$(OutDir)container-$1
HdlContDir=$(call HdlContOutDir,$(call HdlContName,$1,$2))
HdlStripXml=$(if $(filter .xml,$(suffix $1)),$(basename $1),$1)
ifneq ($(MAKECMDGOALS),clean)
#  $(info 0.Only:$(OnlyPlatforms),All:$(HdlAllPlatforms),Ex:$(ExcludePlatforms),HP:$(HdlPlatforms))
  ifndef ShellHdlAssemblyVars
    $(eval $(HdlPrepareAssembly))
  endif
  # default the container names
  $(foreach c,$(Containers),$(eval HdlContXml_$(notdir $c):=$c))
  # $(call HdlConfigConstraints,<container>,<platform>,<config>)
  HdlConfigConstraints=$(strip\
     $(if $(call DoShell,$(call OcpiGenTool) -Y $(HdlPlatformDir_$2)/hdl/$3,HdlConfConstraints),\
          $(error Processing platform configuration XML "$3" for platform "$2" for container "$1"),\
	  $(call HdlGetConstraintsFile,$(HdlConfConstraints),$2)))
  ## Extract the platforms and targets from the containers that have their own xml
  ## Then we can filter the platforms and targets based on that
  ifdef Containers
    # Add a defined non-default container to the build
    # $(call addContainer,<container>,<platform>,<config>,<constraints>)
    define addContainer
      $$(if $$(HdlPart_$2),,\
        $$(error Platform for container $1, $2, is not defined))
      $$(call OcpiDbg,HdlPart_$2:$$(HdlPart_$2))
      HdlMyTargets+=$$(call HdlGetFamily,$$(HdlPart_$2))
      ContFile:=$(notdir $1)
      ContName:=$(Worker)_$2_$3_$$(ContFile)
      HdlContainerImpls_$$(ContFile)+= $$(ContName)
      HdlPlatform_$$(ContName):=$2
      HdlTarget_$$(ContName):=$$(call HdlGetFamily,$$(HdlPart_$2))
      HdlConfig_$$(ContName):=$3
      HdlConstraints_$$(ContName):=$4
      HdlContXml_$$(ContName):=$$(call HdlContOutDir,$$(ContName))/gen/$$(ContName).xml
      $$(shell mkdir -p $$(call HdlContOutDir,$$(ContName))/gen; \
               if ! test -e  $$(HdlContXml_$$(ContName)); then \
                 ln -s ../../$1.xml $$(HdlContXml_$$(ContName)); \
               fi)
      HdlContainers:=$$(HdlContainers) $$(ContName)
    endef
    # This procedure is (simply) to extract platform, target, and configuration info from the
    # xml for each explicit container. This allows us to build the list of targets necessary for
    # all the platforms mentioned by all the containers.  This list then is the default targets
    # when targets are not specified.
    # ocpigen -X returns the config as word 1 followed by "only platforms" as subsequent words
    define doGetPlatform
      $$(and $$(call DoShell,$(call OcpiGen, -X $1),HdlContPfConfig),\
          $$(error Processing container XML $1: $$(HdlContPfConfig)))
      HdlContPlatforms:=$$(wordlist 3,$$(words $$(HdlContPfConfig)),$$(HdlContPfConfig))
      $$(foreach p,$$(filter $$(or $$(HdlContPlatforms),$$(OnlyPlatforms),$$(HdlAllPlatforms)),\
                      $$(filter-out $$(ExcludePlatforms),$$(HdlPlatforms))),\
	 $$(eval $$(call addContainer,$1,$$p,$$(word 1,$$(HdlContPfConfig)),$$(word 2,$$(HdlContPfConfig)))))
    endef
    $(foreach c,$(Containers),$(eval $(call doGetPlatform,$(call HdlStripXml,$c))))
  endif
  # Create the default container directories and files
  # $(call doDefaultContainer,<platform>,<config>)
  HdlDefContXml=<HdlContainer platform='$1/$2' default='true'/>
#  $(info 1.Only:$(OnlyPlatforms),All:$(HdlAllPlatforms),Ex:$(ExcludePlatforms),HP:$(HdlPlatforms))
  define doDefaultContainer
    $(call OcpiDbg,In doDefaultContainer for $1/$2 and HdlPlatforms: $(HdlPlatforms) [filtered=$(filter $1,$(HdlPlatforms))])
    ifneq (,$(if $(HdlPlatforms),$(filter $1,$(HdlPlatforms)),yes))
      # Create this default container's directory and xml file
      ContName:=$(call HdlContName,$1,$2)
      $(foreach d,$(call HdlContDir,$1,$2)/gen,\
        $$(foreach x,$d/$$(ContName).xml,\
          $$(shell \
               mkdir -p $d; \
               if test ! -f $$x || test "`cat $$x`" != "$(call HdlDefContXml,$1,$2)"; then \
                 echo "$(call HdlDefContXml,$1,$2)"> $$x; \
               fi)))
      HdlPlatform_$$(ContName):=$1
      HdlTarget_$$(ContName):=$(call HdlGetFamily,$(HdlPart_$1))
      HdlConfig_$$(ContName):=$2
      HdlContXml_$$(ContName):=$$(call HdlContOutDir,$$(ContName))/gen/$$(ContName).xml
      HdlContainers:=$$(HdlContainers) $$(ContName)
      HdlBaseContainerImpls:=$$(HdlBaseContainerImpls) $$(ContName)
    endif
  endef
  ifeq ($(origin DefaultContainers),undefined)
    $(call OcpiDbg,No Default Containers: HdlPlatforms: $(HdlPlatforms))
    # If undefined, we determine the default containers based on HdlPlatforms
    $(foreach p,$(filter $(or $(OnlyPlatforms),$(HdlAllPlatforms)),\
                 $(filter-out $(ExcludePlatforms),$(HdlPlatforms))),\
                $(eval $(call doDefaultContainer,$p,base)))
    $(call OcpiDbg,No Default Containers: Containers: $(Containers) (Container=$(Container)))
  else
    $(foreach d,$(DefaultContainers),\
       $(if $(findstring /,$d),\
         $(eval \
           $(call doDefaultContainer $(word 1,$(subst /, ,$d)),$(word 2,$(subst /, ,$d)))),\
         $(if $(filter $d,$(filter $(or $(OnlyPlatforms),$(HdlAllPlatforms)),$(filter-out $(ExcludePlatforms),$(HdlAllPlatforms)))),\
              $(eval $(call doDefaultContainer,$d,base)),\
              $(error In DefaultContainers, $d is not a defined HDL platform.))))
  endif
  HdlContXml=$(or $($1_xml),$(if $(filter .xml,$(suffix $1)),$1,$1.xml))
  ifdef HdlContainers
    HdlMyPlatforms:=$(foreach c,$(HdlContainers),$(HdlPlatform_$c))
    ifdef HdlPlatforms
      override HdlPlatforms:=$(call Unique,$(filter $(HdlMyPlatforms),$(HdlPlatforms)))
    else
      override HdlPlatforms:=$(call Unique,$(HdlMyPlatforms))
    endif
    override HdlPlatforms:=$(filter $(or $(OnlyPlatforms),$(HdlAllPlatforms)),$(filter-out $(ExcludePlatforms),$(HdlPlatforms)))
    ifdef HdlTargets
      HdlTargets:=$(call Unique,$(filter $(HdlMyTargets),$(HdlTargets)))
    else
      HdlTargets:=$(call Unique,$(foreach p,$(HdlPlatforms),$(call HdlGetFamily,$(HdlPart_$p))))
    endif
  endif # for ifdef Containers
else # for "clean" goal
  HdlTargets:=all
endif
ifdef HdlContainers
  ifndef ShellHdlAssemblyVars
    include $(OCPI_CDK_DIR)/include/hdl/hdl-worker.mk
  endif
endif
# This is delayed until here since we are modifying platform variables based on containers etc.
include $(OCPI_CDK_DIR)/include/hdl/hdl-pre.mk
# Due to our filtering, we might have no targets to build
ifeq ($(filter $(or $(OnlyPlatforms),$(HdlAllPlatforms)),$(filter-out $(ExcludePlatforms),$(HdlPlatforms)))$(filter skeleton,$(MAKECMDGOALS)),)
  $(and $(HdlPlatforms),$(info Not building assembly $(Worker) for platform(s): $(HdlPlatforms) in "$(shell pwd)"))
#  $(info No targets or platforms to build for this "$(Worker)" assembly in "$(shell pwd)")
else ifndef HdlContainers
  ifneq ($(MAKECMDGOALS),clean)
    $(info No containers will be built since none use the specified platforms.)
  endif
else
  ifndef HdlSkip
    ifndef HdlContainers
      ifneq ($(MAKECMDGOALS),clean)
        $(info No containers will be built since none use the specified platforms.)
      endif
    else
      # We have containers to build locally.
      # We already build the directories for default containers, and we already
      # did a first pass parse of the container XML for these containers
      HdlContResult=$(call HdlContOutDir,$1)/target-$(HdlTarget_$1)/$1.bitz
      # $(call HdlGetContainerConstraints,<container>,<platform>,<config>,<constraints>)
      # If the container does not mention a constraints file, get it from the pf config
      # If the container mentions a constraints file, it, may be local to the
      # container file (in the assembly dir), or under the platform directory
      HdlGetContainerConstraintsFiles=$(infox HGCC:$1:$2:$3:$4)$(strip\
        $(comment figure out the contraints now that hdl-pre.mk has been run for toolsets etc.)\
	$(foreach c,$(if $(filter -,$4),\
                       $(HdlPlatformDir_$2)/$(call HdlConfigConstraints,$1,$2,$3),\
                       $(call HdlGetConstraintsFile,$4,$2)),$(call OcpiInfox,CCC:$c)\
          $(foreach f,$(word 1,$(subst ?, ,$c)),\
            $(or $(wildcard $f),$(strip \
                 $(and $(filter-out /%,$f),$(wildcard $(HdlPlatformDir_$2)/$f))),\
              $(error Constraints file $f not found for container $1))$(foreach e,$(word 2,$(subst ?, ,$c)),?$e))))
      HdlGetContainerConstraints=$(strip \
        $(call HdlGetContainerConstraintsFiles,$1,$(HdlPlatform_$1),$(HdlConfig_$1),$(HdlConstraints_$1)))

      define doContainer
        .PHONY: $(call HdlContOutDir,$1)
        all: $(call HdlContResult,$1)$(infox DEPEND:$(call HdlContResult,$1))
	        $(AT)if [[ -n "${OCPI_ARTIFACTS_ONLY}" ]]; then \
	          echo "Cleaning assembly intermediate build files"; \
	          find $(call HdlContOutDir,$1)/target-* -type f -not \( -name '*.edf' -o \
	                                        -name '*-defs.vh' -o \
	                                        -name '*.bitz' -o \
	                                        -name '*.vdb' \
	                                        \) -exec rm -f {} + 2>/dev/null; \
	          find $(call HdlContOutDir,$1)/target-* -type d -empty -delete; \
            rm -rf $(call HdlContOutDir,$1)/gen gen lib target-$(HdlTarget_$1); \
	        fi

        $(call HdlContResult,$1): links
	        $(AT)mkdir -p $(call HdlContOutDir,$1)
	        $(AT)$(MAKE) -C $(call HdlContOutDir,$1) -f $(OCPI_CDK_DIR)/include/hdl/hdl-container.mk \
	        $(and $(OCPI_PROJECT_REL_DIR),OCPI_PROJECT_REL_DIR=../$(OCPI_PROJECT_REL_DIR)) \
            HdlAssembly=../../$(CwdName)  HdlConfig=$(HdlConfig_$1) \
            HdlConstraints='$(call AdjustRelative,$(call HdlGetContainerConstraints,$1))' \
            HdlPlatforms=$(HdlPlatform_$1) HdlPlatform=$(HdlPlatform_$1) \
            ComponentLibrariesInternal="$(call OcpiAdjustLibraries,$(ComponentLibraries))" \
            HdlExplicitLibraries="$(call OcpiAdjustLibraries,$(HdlExplicitLibraries))" \
            XmlIncludeDirsInternal="$(call AdjustRelative,$(XmlIncludeDirsInternal))"
      endef
      
      ifndef HdlSkip
      $(foreach c,$(HdlContainers),$(and $(filter $(HdlPlatform_$c),$(HdlPlatforms)),$(eval $(call doContainer,$c))))
      endif
    endif # containers
  endif # of skip
endif # check for no targets

clean::
	$(AT) rm -r -f container-* lib

# Expose information about this assembly's containers for use
# in some external application.
#
# Impl is an Implementation of a Container (i.e. the actual build directory)
# An Impl is bound to a Platform and even a Platform Configuration
# TODO: Here we should also provide HdlConfig_* so the actual Platform Configuration
#       mapping is available for each container implementation
ifdef ShellHdlAssemblyVars
shellhdlassemblyvars:
$(info Containers="$(Containers)"; \
       DefaultContainers="$(DefaultContainers)"; \
       HdlContainers="$(HdlContainers)"; \
       HdlBaseContainerImpls="$(HdlBaseContainerImpls)"; \
       $(foreach c,$(Containers),\
	 HdlContainerImpls_$c="$(HdlContainerImpls_$c)"; )\
       $(foreach c,$(HdlContainers),\
         HdlPlatform_$c="$(HdlPlatform_$c)"; )\
        )
endif
